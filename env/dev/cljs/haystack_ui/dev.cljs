(ns ^:figwheel-no-load haystack-ui.dev
  (:require [haystack-ui.core :as core]
            [haystack-ui.haystack :as haystack]
            [figwheel.client :as figwheel :include-macros true]))

(enable-console-print!)

(println "in dev.cljs")
(println haystack/websocket-host)

(figwheel/watch-and-reload
  ;; :websocket-url "ws://localhost:3449/figwheel-ws"
 :websocket-url (str "ws://" haystack/websocket-host ":3449/figwheel-ws")
 ;;"ws://finch.insummit.com:3449/figwheel-ws"
  ;; :websocket-url (str "ws://" System/getenv("HOST") ":3449/figwheel-ws")
  :jsload-callback core/mount-root)

(core/init!)

(ns haystack-ui.search-page
  (:require [clojure.string :as string]
            [reagent.core :as r :refer [atom]]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [accountant.core :as accountant]

            [haystack-ui.haystack :as haystack :refer [growl elapsed-growl start-timer lap-time stop-timer get-search timed? search-url scheme host]]
            ))

;; these should not cause re-rendering, so make as normal atoms.
(def manufacturer-dictionary (atom {}))
(def categoory-dictionary (atom {}))

(def feedback-text (r/atom ""))
(def feedback-store (r/atom nil))

(defn raw-html
  [code]
  {:dangerouslySetInnerHTML {:__html code}})

(defn raw-field
  [doc label]
  (raw-html (label doc)))

(defn do-search
  [mappings query-map]
  (start-timer)
  (haystack/get-search mappings @query-map))

(defn do-new-search
  [mappings query-map]
  (swap! query-map assoc :page-num 1)
  (do-search mappings query-map))

(defn do-feedback
  [query-map params]
  (haystack/post-feedback feedback-store
                          (merge
                           params
                           {:feedback @feedback-text
                            :search-text (-> @query-map :search)
                            ;; :category-path (or (-> @query-map :category-path) "")
                            :category-path (-> @query-map :category-path)
                            :manufacturer-ids (str "[" (->> @query-map :manufacturer-ids (string/join ",")) "]")
                            :query-entire (-> @query-map :query-entire)
                            }))
  (reset! feedback-text ""))

(defn show-feedback-aux
  [query-map]
  [:div.feedback
   ;; [:b "Expected: "]
   [:input
    {:name "expected"
     :placeholder "What matnr(s) should have appeared?"
     :type "text"
     :value @feedback-text
     :style {:width "250px"}
     ;; :autocorrect="off"
     ;; :autoComplete "off"
     :on-change (fn [event]
                  (let [value (-> event .-target .-value)]
                    (reset! feedback-text value)))
     ;; :on-key-down #(when (= (.-keyCode %) 13)
     ;;                 (do-new-search mappings query-map))
     }]
   [:br]
   [:input
    {:name "good"
     :type "button"
     :value "Great Results"
     :style {:color "white" :background-color "green" :margin-right "20px"}
     :on-click #(do-feedback query-map {:rating "great"})
     }]
   [:input
    {:name "ok"
     :type "button"
     :value "Fair"
     :style {:color "black" :background-color "yellow" :margin-right "20px"}
     :on-click #(do-feedback query-map {:rating "fair"})
     ;; :style {:color "black" :background-color "#e6e600"}
     }]
   [:input
    {:name "poor"
     :type "button"
     :value "Poor"
     :style {:color "white" :background-color "red"}
     :on-click #(do-feedback query-map {:rating "poor"})
     }]
   ])

(defn show-feedback
  [query-map]
  [:div
   [show-feedback-aux query-map]
   ])

(defn set-manufacturer
  [mappings query-map selection]
  (let [id (:key selection)
        ids (:manufacturer-ids @query-map)
        selected? (contains? ids id)
        ids (if selected?
              (disj ids id)
              (conj ids id))]
    (swap! query-map assoc :manufacturer-ids ids)))

(defn show-manufacturer
  [mappings query-map selection]
  [:div
   [:input.toggle {:type "checkbox" :checked (contains? (:manufacturer-ids @query-map) (:key selection))
                   :on-change #(do
                                 (set-manufacturer mappings query-map selection)
                                 (do-new-search mappings query-map))}]
   " "
   [
    :label.inline {
                   :on-click #(do
                                (set-manufacturer mappings query-map selection)
                                (do-new-search mappings query-map))
                   :on-double-click #(growl (:key selection))}
    (:name selection) " "
    [:span.badge.badge-float (:doc_count selection)]
    ]]
  )

(defn show-manufacturers
  [mappings query-map options]
  (doall (for [m (->> @mappings :aggregations :manufacturer-id)]
           (swap! manufacturer-dictionary assoc (:key m) m)))
  (let [selections (->> @mappings :aggregations :manufacturer-id)
        selected-manufacturer-ids (:manufacturer-ids @query-map)
        ;; selected-manufacturers (map #(assoc (@manufacturer-dictionary %) :doc_count 0) selected-manufacturer-ids)
        selected-manufacturers (reduce #(assoc %1 %2 (assoc (@manufacturer-dictionary %2) :doc_count 0)) {} selected-manufacturer-ids)
        selected-manufacturers (reduce #(if (contains? selected-manufacturer-ids (:key %2)) (assoc %1 (:key %2) %2) %1) selected-manufacturers selections)
        selected-manufacturers (vals selected-manufacturers)
        ;; selected-manufacturers {}
        ;; selected-manufacturers (merge
        ;;                         selected-manufacturers
        ;;                         (filter #(contains? selected-manufacturer-ids (:key %)) selections))
        unselected-manufacturers (remove #(contains? selected-manufacturer-ids (:key %)) selections)
        show-all? (:show-all-manufacturers? @options)
        show-count (if (or show-all? (< (count unselected-manufacturers) 12)) (count unselected-manufacturers) 10)
        ;; selections (->> unselected-manufacturers (take show-count) (sort-by :name))
        ]
    ;; (growl selected-manufacturer-ids)
    ;; (growl (filter #(contains? selected-manufacturer-ids (:key %)) selections))
    ;; (growl selected-manufacturers)

    (when (or (not (empty? selections)) (not (empty? selected-manufacturers)))
      [:div
       [:h5.heading "Manufacturers"]
       [:span
        (when-not (empty? selected-manufacturers)
          ;; (doall
           (for [selection (sort-by :name selected-manufacturers)]
             ^{:key (:key selection)} [show-manufacturer mappings query-map selection]) ;)
          )]
       [:span
        (when-not (empty? unselected-manufacturers)
          ;; (doall
           (for [selection (->> unselected-manufacturers (take show-count) (sort-by :name))]
             ^{:key (:key selection)} [show-manufacturer mappings query-map selection]) ;)
          )]
       ;; (doall
       ;;  (for [selection selections]
       ;;    ^{:key (:key selection)}
       ;;    [:div
       ;;     [:input.toggle {:type "checkbox" :checked (contains? (:manufacturer-ids @query-map) (:key selection))
       ;;                     :on-change #(do
       ;;                                   (selection-fn mappings query-map selection)
       ;;                                   (do-new-search mappings query-map))}]
       ;;     " "
       ;;     [
       ;;      :label.inline {
       ;;                     :on-click #(do
       ;;                                  (selection-fn mappings query-map selection)
       ;;                                  (do-new-search mappings query-map))
       ;;                     :on-double-click #(growl (:key selection))}
       ;;      (:name selection) " "
       ;;      [:span.badge.badge-float (:doc_count selection)]
       ;;      ]]
       ;;    ;; [:p [:a {:on-click #(when selection-fn
       ;;    ;;                                ;(growl (:key selection))
       ;;    ;;                       (selection-fn mappings query-map selection)
       ;;    ;;                       (do-search mappings query-map))} (:name selection) " " [:span.badge (:doc_count selection)]]]
       ;;    ))
       [:a.pull-right {:on-click #(swap! options assoc :show-all-manufacturers? (not show-all?)) :on-double-click #(growl :right)} (if show-all? "Less..." "More...")]
       ;; [:p (str selections)]
       ])))

(defn show-aggregation
  [title key selection-fn mappings query-map]
  (let [selections (-> @mappings :aggregations key)
        selections (sort-by :name selections)
        ]
    (when (not (empty? selections))
      [:div
       [:h5.heading title]
       (for [selection selections]
         ^{:key (:key selection)}
         [:span [:a.black.narrow {:on-click #(when selection-fn
                                        ;(growl (:key selection))
                               (selection-fn mappings query-map selection)
                               (do-new-search mappings query-map))} (:name selection) " " [:span.badge (:doc_count selection)]]
          [:hr.narrow]])
       ;; [:p (str selections)]
       ])))

(defn set-category-path
  [mappings query-map selection]
  (let [path (:key selection)
        path (if (empty? path) nil path)]
    (swap! query-map assoc :category-path path)))

;; [show-aggregation "Category Breadcrumbs" :category-path-ancestors set-category-path mappings query-map]
(defn show-category-ancestors
  [mappings query-map]
  (let [key :category-path-ancestors
        selection-fn set-category-path
        selections (-> @mappings :aggregations key)]
    (when (not (empty? selections))
      [:div
       (let [selection (first selections)]
         [:span.black
          [:a.category-ancestor.black {:on-click #(when selection-fn
                                  (selection-fn mappings query-map selection)
                                  (do-new-search mappings query-map))} (:name selection) " " [:span.badge (:doc_count selection)]]])
       (for [selection (rest selections)]
         ^{:key (:key selection)}
         [:span.black
          ;; [:span {:style "margin: 0px 4px;"} " > "]
          [:span " "]
          [:span.icon.icon-arrow-right.bc-arrow]
          [:span " "]
          [:a.category-ancestor.black {:on-click #(when selection-fn
                            (selection-fn mappings query-map selection)
                            (do-new-search mappings query-map))} (:name selection) " " [:span.badge (:doc_count selection)]]
          ])
       [:br]
       [:br]
       ;; [:p (str selections)]
       ])))

(defn show-prev-next
  [mappings query-map]
  (let [page-num (:page-num @query-map)
        num-pages (-> @mappings :paging :num-pages)]
    [:div
     (if (< 1 page-num)
       [:a {:on-click #(do
                         (swap! query-map assoc :page-num (dec page-num))
                         (do-search mappings query-map))} "<prev"])
     ;; [:span {:style "margin-right: 5px;"} " "]
     " "
     [:b.lightgray (str page-num " of " num-pages)]
     " "
     (if (< page-num num-pages)
       [:a {:on-click #(do
                         (swap! query-map assoc :page-num (inc page-num))
                         (do-search mappings query-map))} "next>"])
     ]))

(defn show-sidebar
  [mappings query-map options]
  (let [category-ancestors (-> @mappings :aggregations :category-path-ancestors)
        category-title (if (not (empty? category-ancestors))
                         (-> category-ancestors last :name)
                         "Categories")]
    [:div.left-pane.column
     ;; [:div (-> @query-map str)]
     [show-aggregation category-title :category-path set-category-path mappings query-map]
     [show-manufacturers mappings query-map options]
     [:div
      [:a {:href (str scheme "://" host @haystack/search-url) :target "_blank"} "request"]]
     ]))

(defn nix-search-word
  [mappings query-map token]
  (growl token)
  (let [tokens (-> @query-map :search string/trim (string/split #"\s+"))]
    (swap! query-map assoc :search (string/join " " (remove #(= token %) tokens)))
    (do-new-search mappings query-map)
    ))

(defn show-search-input
  [mappings query-map]
  (let [search-text (:search @query-map)
        ;; search-text (if (string/blank? search-text) search-text (str search-text " "))
        ]
    [:div.content-pane.float-right
     [:div  ;:nav.top-menu
      ;; [:img.summit-logo
      ;;  {:alt "Summit Electric Supply" :src "/assets/modern-theme/summit-logo-9cb2cbbe7e6dd9ced0065ff02d3226ffc5bcc406481ddf4abbcc6961393f936e.png" :title "Summit Home Page"}]
      [show-category-ancestors mappings query-map]
      [:b "Search: "]
      [:input.appendedInputButton
       {:name "search"
        :placeholder "Enter Product Search Terms"
        :type "search"
        :autocorrect="off"
        :autoComplete "off"
        :auto-focus true
        :autofocus true
        :value search-text
        :on-change (fn [event]
                     (let [value (-> event .-target .-value)]
                       (swap! query-map assoc-in [:search] value)))
        :on-key-down #(when (= (.-keyCode %) 13)
                        (let [text (:search @query-map)
                              text (if (string? text) (string/trim text) "")
                              text (if (string/blank? text) text (str text " "))]
                          (swap! query-map assoc-in [:search] text)
                          (do-new-search mappings query-map)))
        }
       ]
      [:input
       {:name "reset-search"
        :type "button"
        :value "x"
        :style {:color "white" :background-color "lightgray" :padding "2px" :display "inline" :margin-bottom "10px"}
        :on-click #(do
                     (swap! query-map assoc :search "" :category-path "" :manufacturer-ids #{})
                     (do-new-search mappings query-map))
        }]
      [:div {:style {:margin "-4px 4px 3px 0px"}}
       (doall
        (for [token (set (string/split (string/trim search-text) #"\s+"))]
          (when-not (string/blank? token)
            ^{:key token}
            [:a.badge {:style {:margin "0px 8px 3px 0px"}
                       :on-click #(nix-search-word mappings query-map token)} (str "x " (pr-str token))])))]
      ]
     ])
  )

(defn show-document
  [doc]
  (let [raw (partial raw-field doc)
        product-url (str "https://www.summit.com/store/products/" (:id doc))]
    [:div.product-result.media
     ;; [:a {:href "https://www.google.com/"} "google"]
     [:a.pull-left.product-image-link {:href product-url :target "_blank"}
      ;; [:img.product-thumb.img-rounded {:alt (:name doc) :src (:image-url doc)}]]
      [:img.product-thumb.img-rounded {:alt "" :src (:image-url doc)}]]
     [:div.media-body
      ;; [:h3 [:a {:href  :target "_blank"} (raw :name)]]
      [:h3.h3 [:a (merge {:href product-url :target "_blank"} (raw :name))]]
      [:p (raw :description)]
      [:div.muted
       [:div.field-container
        [:span.field-label "Manufacturer: "]
        [:span.field-value (raw :manufacturer-name)]]
       [:div.field-container
        [:span.field-label "Manufacturer Part #: "]
        [:span.field-value (raw :manufacturer-part-number)]]
       [:div.field-container
        [:span.field-label "Summit Part #: "]
        [:span.field-value (raw :summit-part-number)]]
       [:div.field-container
        [:span.field-label "UPC: "]
        [:span.field-value (raw :upc)]]
       [:div.field-container
        [:span.field-label "Matnr: "]
        [:span.field-value (raw :matnr)]]
       [:div.field-container
        [:span.field-label "Category: "]
        [:span.field-value (raw :category-name)]]
       [:div.field-container
        [:span.field-label "Product Class: "]
        [:span.field-value (raw :product-class)]]
       ]
      ]
     ]))

(defn show
  [mappings query-map options]
  (let [entire? (-> @query-map :query-entire)]
    [:div.container.clearfix
     [show-feedback query-map]
     ;; [:div
     [show-sidebar mappings query-map options]
     [:div.search-list.column
      ;; [:h2 "Welcome to Haystack!"]
      [show-search-input mappings query-map]
      ;; [:div [:a {:href "/about"} "go to about page"]]
      ;; [:div (str (if @mappings (keys @mappings) "loading"))]
      ;; [document (-> @mappings :documents first)]
      [:div
       [:p
        [:span [:a.black {
                    :on-click #(do
                                 (swap! query-map assoc :query-entire false)
                                 (do-new-search mappings query-map))
                    :class (if entire? "not-selected-stock" "selected-stock")}
                "Albuquerque " [:span.badge (-> @mappings :paging :local-item-count)]]]
        [:span [:a.black {:on-click #(do
                                       (swap! query-map assoc :query-entire true)
                                       (do-new-search mappings query-map))
                          :on-double-click #(growl "double")
                          :class (if entire? "selected-stock" "not-selected-stock")}
         "All Products " [:span.badge (-> @mappings :paging :entire-item-count)]]]
        ]
       [:p ""]
       ]
      ;; (for [doc (:documents @mappings)]
      ;;   ^{:key (:id doc)}
      ;;   [show-document doc])
      (let [docs (:documents @mappings)]
        (if (or (not docs) (empty? docs))
          [:h2 "No results"]
          [:div
            (doall
             (for [doc (:documents @mappings)]
               ^{:key (:id doc)}
               [show-document doc]))
            ;; [:div (str (if @mappings @mappings "loading"))]
            [show-prev-next mappings query-map]]))
      (when (timed?)
        (elapsed-growl "finished show")
        (stop-timer)
        nil)
      ]])
  )


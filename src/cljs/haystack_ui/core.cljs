(ns haystack-ui.core
    (:require [reagent.core :as r :refer [atom]]
              [reagent.session :as session]
              [secretary.core :as secretary :include-macros true]
              [accountant.core :as accountant]

              [haystack-ui.haystack :as haystack :refer [growl]]
              [haystack-ui.search-page :as search]
              ))

;; -------------------------
;; Views

(defn home-page []
  (let [mappings (r/atom nil)
        options (r/atom {:show-all-manufacturers? false})
        query-map (r/atom {:search ""
                           :category-path nil
                           :manufacturer-ids #{}
                           :service-center-id 7
                           :query-entire true
                           :page-num 1
                           :num-per-page 25
                           })]
    ;; (haystack/get-mappings mappings)
    ;; (growl "homepage")
    ;; (growl @query-map)
    (haystack/get-search mappings @query-map)
    (fn home-page-fn []
      [:div.content-wrapper
       [:div.wrapper
        ;; <a class="home" href="/" id="summit-logo">
        ;; <img alt="Summit Electric Supply" class="summit-logo" id="summit-logo" src="/assets/modern-theme/summit-logo-9cb2cbbe7e6dd9ced0065ff02d3226ffc5bcc406481ddf4abbcc6961393f936e.png" title="Summit Home Page">
        [:div.row-fluid
         [:div.span12.page-body
          [search/show mappings query-map options]
           ]]]])))

(defn about-page []
  [:div [:h2 "About haystack-ui"]
   [:div [:a {:href "/"} "go to the home page"]]])

(defn current-page []
  [:div [(or (session/get :current-page) #'home-page)]])

;; -------------------------
;; Routes

(secretary/defroute "/" []
  (session/put! :current-page #'home-page))

(secretary/defroute "/about" []
  (session/put! :current-page #'about-page))

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (mount-root))

;; interface to the haystack backend

(ns haystack-ui.haystack
  ;; (:require-macros [reagent.ratom :refer [reaction]]
  (:require [reagent.core :as r]
            [clojure.walk :refer [keywordize-keys]]
            ;; [siren.core :refer [siren! sticky-siren! base-style]]
            [siren.core :as siren :refer [siren! sticky-siren!]]
            [ajax.core :refer [GET POST]]
            [clojure.string :as string]
            [cljs.pprint :as pprint]
            ;; [cljsjs.fixed-data-table]

            ;; [murphydye.utils.core :as utils :refer [ppc]]
            ;; [murphydye.websockets :as ws]
            ;; [murphydye.window :as win]

            ))

(def search-url (r/atom nil))


(if false
  (do
    (def scheme "https")
    (def haystack-host "www.murphydye.com")
    (def haystack-port "")
    ;; (def websocket-host haystack-host)
    (def websocket-host "10.9.0.124")
    )

  (do
    (def scheme "http")
    ;; (def haystack-host "10.9.0.139")
    ;; (def haystack-host "10.9.0.124")
    ;; (def haystack-host "192.168.0.210")
    (def haystack-host "192.168.0.212")
    ;; (def haystack-host "finch.insummit.com")
    (def haystack-port ":8080")

    (def websocket-host haystack-host)
    ;; (def websocket-host "10.9.0.124")
    ))


;; (def host "localhost:8080")
(def host (str haystack-host haystack-port))
;; (def prefix "/api/v2/")
(def prefix "/")

(def timer (atom nil))
(defn seconds
  []
  (/ (.getTime (js/Date.)) 1000))
(defn start-timer
  []
  (reset! timer (seconds)))
(defn reset-timer
  []
  (reset! timer nil))
(defn lap-time
  []
  (when @timer
    (- (seconds) @timer)))
(defn timed?
  []
  (not (nil? @timer)))
(defn stop-timer
  []
  (let [elapsed (lap-time)]
    (reset-timer)
    elapsed))

(defn growl
  [msg]
  (siren! {:content (str msg)})
  nil)

(defn elapsed-growl
  [msg]
  (let [secs (lap-time)]
    (if secs
      (growl (str msg " (" (pprint/cl-format nil  "~,3f" secs) ")"))
      (growl msg))))

(defn handler [response]
  (.log js/console (str response)))

(defn create-feedback-success
  ([store]
   (fn [response]
     (elapsed-growl "Thank you for your feedback!")
     (reset! store (if (map? response) (keywordize-keys response) response)))))

(defn create-handler
  ([store]
   (fn [response]
     (elapsed-growl "ajax finished")
     (reset! store (if (map? response) (keywordize-keys response) response)))))

(defn error-handler [{:keys [status status-text]}]
  (.log js/console (str "something bad happened: " status " " status-text))
  (growl (str "Error during ajax call: " status " " status-text)))

(defn create-error-handler
  [store]
  (fn [{:keys [status status-text]}]
    (let [msg (str "Error during ajax call: " status " " status-text)]
      (.log js/console msg)
      (growl msg)
      (reset! store msg)
      )))

(defn ajax-post
  ([store url]
   (ajax-post store url {}))
  ([store url params]
   (let [options {:timeout 240000  ;; 2 minutes
                  :handler (create-feedback-success store)
                  :response-format :json
                  :error-handler (create-error-handler store)
                  :format :json
                  :params params}
         url (str scheme "://" host prefix url)
         ]
     ;; (growl (str options))
     ;; (growl url)
     ;; (growl params)
     (POST url options))))

(defn ajax-get
  ([store url]
   (ajax-get store url {}))
  ([store url params]
   (let [options {:timeout 240000  ;; 2 minutes
                  :handler (create-handler store)
                  ;; :response-format "application/transmit+json"
                  :response-format :json
                  :error-handler (create-error-handler store)
                  :params params}
         url (str scheme "://" host prefix url)
         ]
     ;; (growl url)
     (GET url options))))

(defn post-feedback
  [store params]
  (ajax-post store "api/v2/feedback"
             (merge
              params
              {:app "search-ui"
               })))

(defn get-mappings
  [store]
  (ajax-get store "admin/mappings"))

(defn get-search
  [store query-map]
  (let [ids (:manufacturer-ids query-map)
        query-map (if (empty? ids)
                    (dissoc query-map :manufacturer-ids)
                    (assoc query-map :manufacturer-ids (str "[" (string/join "," (map str ids)) "]"))
                    )]
    (reset! search-url (str "/api/v2/search?" (string/join "&" (map (fn [[k v]] (str (name k) "=" v)) query-map))))

    (ajax-get store "api/v2/search" query-map)))

